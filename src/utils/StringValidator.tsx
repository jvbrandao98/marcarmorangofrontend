import React from 'react';

export function capitalize(str: string) {
    if (str === undefined || str === null || str.length <= 0) {
        return null;
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
}
