import React, { useEffect, useState } from 'react';
import { Box, Paper, Typography, Grid, TextField, Button, Container } from '@material-ui/core';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import strawberries from '../Assets/strawberries.jpg';
import LoginApi from '../api/requestApi/LoginApi';

export const StyledContainer = styled(Container)`
    &&& {
        height: 100vh;
        width: 100%;
        display: flex;
        background-image: url(${strawberries});
        background-size: cover;
        max-width: none;
    }
`;

const Login = () => {
    const navigate = useNavigate();
    const { loginUser, successLogin } = LoginApi();

    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');

    const handleLogin = (e: any) => {
        e.preventDefault();
        const payload = {
            email: userEmail,
            password: userPassword
        };
        loginUser(payload);
    };

    const handleRegisterNewUser = () => {
        navigate('/register');
    };

    useEffect(() => {
        successLogin && navigate('/home');
    }, [successLogin]);

    return (
        <StyledContainer>
            <Grid
                item
                xl={4}
                sm={12}
                lg={4}
                md={5}
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Paper style={{ height: '80%' }}>
                    <Box
                        style={{
                            display: 'flex',
                            height: '18%',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Typography variant="h6">Faça seu login</Typography>
                    </Box>
                    <Box style={{ height: '82%' }}>
                        <Box
                            style={{
                                height: '50%',
                                padding: '32px 32px 0px 32px'
                            }}
                        >
                            <TextField onChange={(event) => setUserEmail(event.target.value)} style={{ marginBottom: '16px', width: '100%' }} label="Email" variant="outlined" />
                            <TextField onChange={(event) => setUserPassword(event.target.value)} type="password" label="Senha" variant="outlined" style={{ width: '100%' }} />
                            <Box>
                                <Button
                                    style={{
                                        border: '1px solid black',
                                        marginTop: '16px',
                                        width: '100%'
                                    }}
                                    onClick={handleLogin}
                                >
                                    Logar
                                </Button>
                            </Box>
                        </Box>
                        <Box
                            style={{
                                height: '25%',
                                display: 'grid',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Box>
                                <Button>Esqueceu sua senha?</Button>
                            </Box>
                            <Box>
                                <Button onClick={handleRegisterNewUser}>Criar nova conta</Button>
                            </Box>
                        </Box>
                    </Box>
                </Paper>
            </Grid>
            <Grid item xl={7} sm={7} lg={7}></Grid>
        </StyledContainer>
    );
};

export default Login;
