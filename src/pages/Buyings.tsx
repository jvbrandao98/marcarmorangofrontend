import React from 'react';

export interface IAboutPageProps {}

const BuyingsPage: React.FunctionComponent<IAboutPageProps> = () => {
    return (
        <div>
            <p>This is the compras page.</p>
        </div>
    );
};

export default BuyingsPage;
