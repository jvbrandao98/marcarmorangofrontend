import React, { useEffect, useReducer, useState } from 'react';
import RegisterOperationApi from '../api/requestApi/RegisterOperation';
import GetSellListApi from '../api/requestApi/SellList';
import ButtonForm from '../components/Button';
import BuyingDialogForm from '../components/Forms/BuyingForm';
import SellingDialogForm from '../components/Forms/SellingForm';

export interface IHomePageProps {}

const formReducer = (state: any, event: any) => {
    return {
        ...state,
        [event.name]: event.value
    };
};

const HomePage: React.FunctionComponent<IHomePageProps> = () => {
    const [openBuyingForm, setOpenBuyingForm] = useState(false);
    const [openSellingForm, setOpenSellingForm] = useState(false);
    const [formDataSell, setFormDataSell] = useReducer(formReducer, {});
    const { registerOperation, successRegistrationOperation } = RegisterOperationApi();
    const { sellListData, sellList } = GetSellListApi();

    const handleOpenSellingForm = () => {
        setOpenSellingForm(!openSellingForm);
    };

    const handleOpenBuyingForm = () => {
        setOpenBuyingForm(!openBuyingForm);
    };

    const handleCloseSelling = () => {
        setOpenSellingForm(false);
    };

    const handleCloseBuying = () => {
        setOpenBuyingForm(false);
    };

    const submitBuyingForm = () => {
        console.log('eita comprei');
    };

    const submitSellingForm = () => {
        const payloadSell = {
            nome: 'Charmander',
            data: '2022-03-19',
            tipoMorango: 'CAMAROSA',
            qtdMorango: 10,
            precoCaixaMorango: 3.6,
            vendaMorango: 1,
            observacoes: 'Morango está congelado.',
            ativo: true
        };
        registerOperation(payloadSell);
    };

    const deslogar = () => {
        localStorage.removeItem('accessToken');
        window.location.reload();
    };

    useEffect(() => {
        sellListData();
        console.log(' sellListData();', sellList);
    }, []);

    return (
        <div>
            <ButtonForm action={handleOpenBuyingForm} name="Botão do Jean" colour="primary" />
            <ButtonForm action={handleOpenSellingForm} name="Botão do Joao" colour="secondary" />

            <BuyingDialogForm
                openDialogForm={openBuyingForm}
                handleCloseDialog={handleCloseBuying}
                submitForm={submitBuyingForm}
            />
            <SellingDialogForm
                setFormData={setFormDataSell}
                openDialogForm={openSellingForm}
                handleCloseDialog={handleCloseSelling}
                submitForm={submitSellingForm}
            />
            <ButtonForm action={deslogar} name="Deslogar" colour="primary" />
        </div>
    );
};

export default HomePage;
