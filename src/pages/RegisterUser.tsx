import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

import { Box, Paper, Typography, Grid, TextField, Button, Container } from '@material-ui/core';

import strawberries from '../Assets/strawberries.jpg';
import RegisterNewUserApi from '../api/requestApi/RegisterUser';

export const StyledContainer = styled(Container)`
    &&& {
        height: 100vh;
        width: 100%;
        display: flex;
        background-image: url(${strawberries});
        background-size: cover;
        max-width: none;
    }
`;

const ErrorMessage = styled.p`
    color: red;
    font-size: 12px;
`;

const RegisterUser = () => {
    const navigate = useNavigate();
    const { registerUser, successRegistration } = RegisterNewUserApi();

    const [userName, setUserName] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const [userConfirmPassword, setConfirmPassword] = useState('');
    const isDifferentPassword = userPassword !== userConfirmPassword;

    const handleRegisterNewUser = (e: any) => {
        e.preventDefault();
        const payload = {
            username: userName,
            email: userEmail,
            password: userPassword
        };
        registerUser(payload);
        successRegistration && navigate('/home')
    };

    return (
        <StyledContainer>
            <Grid
                item
                xl={4}
                sm={12}
                lg={4}
                md={5}
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Paper style={{ height: '80%' }}>
                    <Box
                        style={{
                            display: 'flex',
                            height: '18%',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Typography variant="h6">Faça seu cadastro</Typography>
                    </Box>
                    <form onSubmit={handleRegisterNewUser}>
                        <Box
                            style={{
                                height: '50%',
                                padding: '32px 32px 0px 32px'
                            }}
                        >
                            <TextField onChange={(event) => setUserName(event.target.value)} style={{ marginBottom: '16px', width: '100%' }} label="Usuário" variant="outlined" />
                            <TextField onChange={(event) => setUserEmail(event.target.value)} label="Email" variant="outlined" style={{ width: '100%', marginBottom: '16px' }} />
                            <TextField onChange={(event) => setUserPassword(event.target.value)} type="password" label="Senha" variant="outlined" style={{ width: '100%', marginBottom: '16px' }} />
                            <>
                                <TextField onChange={(event) => setConfirmPassword(event.target.value)} type="password" label="Confirmar senha" variant="outlined" style={{ width: '100%' }} />
                                {isDifferentPassword && <ErrorMessage>Senhas não correspondem.</ErrorMessage>}
                            </>
                            <Box>
                                <Button
                                    style={{
                                        border: '1px solid black',
                                        marginTop: '16px',
                                        width: '100%'
                                    }}
                                    type="submit"
                                    disabled={isDifferentPassword}
                                >
                                    Cadastrar
                                </Button>
                            </Box>
                        </Box>
                    </form>
                </Paper>
            </Grid>
            <Grid item xl={7} sm={7} lg={7}></Grid>
        </StyledContainer>
    );
};

export default RegisterUser;
