import axios from 'axios';

export default axios.create({
    baseURL: 'http://restooth.herokuapp.com/api/'
})