const endpoints = {
    RegisterUrl:'/auth/register',
    LoginUrl: 'auth/login',
    RegisterOperation: 'noteberry/movimentacoes',
    SellListUrl: 'noteberry/movimentacoes/vendas'
}

export default endpoints;