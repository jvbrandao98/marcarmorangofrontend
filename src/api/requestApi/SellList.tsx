import { useState } from 'react';
import endpoints from '../endpoint/Endpoints';
import axios from '../axios';

const GetSellListApi = () => {
    const [successSellList, setSuccessSellList] = useState(false);
    const [sellList, setSellList] = useState();

    const sellListData = async () => {
        try {
            const response = await axios.get(endpoints.SellListUrl, {
                headers: { authorization: localStorage.getItem('accessToken') as string }
            });
            setSuccessSellList(true);
            setSellList(response.data);
            return response.data;
        } catch (err) {
            console.log('error');
        }
    };
    return {
        sellListData,
        sellList,
        successSellList
    };
};

export default GetSellListApi;
