import React, { useState } from 'react';
import endpoints from '../endpoint/Endpoints';
import axios from '../axios';

const LoginApi = () => {
    const [successLogin, setSuccessLogin] = useState(false);

    const loginUser = async (payload: {}) => {
        try {
            const response = await axios.post(endpoints.LoginUrl, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json' } });
            setSuccessLogin(true);
            localStorage.setItem('accessToken', response.data.token);
            return response.data;
        } catch (err) {
            console.log('error');
        }
    };
    return {
        loginUser,
        successLogin
    };
};

export default LoginApi;
