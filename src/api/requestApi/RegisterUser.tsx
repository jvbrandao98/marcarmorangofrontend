import React, { useState } from 'react';
import endpoints from '../endpoint/Endpoints';
import axios from '../axios';

const RegisterNewUserApi = () => {
    const [successRegistration, setSuccessRegistration] = useState(false);

    const registerUser = async (payload: {}) => {
        try {
            const response = await axios.post(endpoints.RegisterUrl, JSON.stringify(payload), {
                headers: { 'Content-Type': 'application/json' }
            });
            setSuccessRegistration(true);
            return response.data;
        } catch (err) {
            console.log('error');
        }
    };
    return {
        registerUser,
        successRegistration
    };
};

export default RegisterNewUserApi;
