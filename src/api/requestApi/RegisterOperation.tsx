import { useState } from 'react';
import endpoints from '../endpoint/Endpoints';
import axios from '../axios';

const RegisterOperationApi = () => {
    const [successRegistrationOperation, setSuccessRegistrationOperation] = useState(false);

    const registerOperation = async (payload: {}) => {
        try {
            const response = await axios.post(endpoints.RegisterOperation, payload, {
                headers: { authorization: localStorage.getItem('accessToken') as string }
            });
            setSuccessRegistrationOperation(true);
            return response.data;
        } catch (err) {
            console.log('error');
        }
    };
    return {
        registerOperation,
        successRegistrationOperation
    };
};

export default RegisterOperationApi;
