import React from 'react';
import { Navigate } from 'react-router-dom';
import TopNavbar from './components/NavBar/TopNavbar';

const PrivateRoute = ({ children }) => {
    const isAuth = localStorage.getItem('accessToken');
    console.log("caiu aqui?")

    if (isAuth) {
        return (
            <div style={{ display: 'flex', height: '100vh' }}>
                <div style={{ width: '100%' }}>
                    <TopNavbar />
                    {children}
                </div>
            </div>
        );
    } else return <Navigate to="/login" />;
};

export default PrivateRoute;
