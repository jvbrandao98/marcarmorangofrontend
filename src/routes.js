import { Suspense, useEffect } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import HomePage from './pages/Home';
import Login from './pages/Login';
import RegisterUser from './pages/RegisterUser';
import SalesPage from './pages/Sales';
import BuyingsPage from './pages/Buyings';
import PrivateRoute from './privateRoutes';
import { useNavigate } from 'react-router-dom';

const ProjectRoutes = () => {
    const navigate = useNavigate();

  /*   useEffect(() => {
        const isAuth = localStorage.getItem('accessToken');
        isAuth && navigate('/home');
    }, []); */

    return (
        <>
            <Suspense fallback={<div></div>}>
                <Routes>
                    <Route path="/" element={<Navigate replace to="/login" />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<RegisterUser />} />
                    <Route
                        path="/home"
                        element={
                            <PrivateRoute>
                                <HomePage />
                            </PrivateRoute>
                        }
                    />
                    <Route
                        path="/vendas"
                        element={
                            <PrivateRoute>
                                <SalesPage />
                            </PrivateRoute>
                        }
                    />
                    <Route
                        path="/compras"
                        element={
                            <PrivateRoute>
                                <BuyingsPage />
                            </PrivateRoute>
                        }
                    />
                </Routes>
            </Suspense>
        </>
    );
};

export default ProjectRoutes;
