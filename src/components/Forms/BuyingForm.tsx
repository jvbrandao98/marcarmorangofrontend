import React, { useState } from 'react';
// import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
// import ListItemText from '@material-ui/core/ListItemText';
// import ListItem from '@material-ui/core/ListItem';
// import List from '@material-ui/core/List';
// import Divider from '@material-ui/core/Divider';
import styled from 'styled-components';
import ButtonForm from '../Button';
import { Box, DialogContent, DialogTitle, TextField } from '@material-ui/core';


const DialogStyled = styled(Dialog)`
&& .MuiPaper-root {
    width: 100%;
    height: 80%;
}
`;

interface FormProps {
    openDialogForm: boolean;
    handleCloseDialog: () => void;
    submitForm: () => void;
};

const BuyingDialogForm: React.FC<FormProps> = ({openDialogForm, handleCloseDialog, submitForm}) => {

    return(
        <DialogStyled open={openDialogForm} onClose={handleCloseDialog}>
            <DialogTitle>Registrar Venda</DialogTitle>
            <DialogContent style={{ justifyContent: 'center', display: 'flex' }}>
                <Box style={{ display: 'grid' }} component="form" onSubmit={submitForm}>
                    <TextField style={{ width: '100%' }} label="Nome do Comprador" variant="outlined" />
                    <TextField style={{ width: '100%' }} label="Tipo do Morango" variant="outlined" />
                    <TextField style={{ width: '100%' }} label="Quantidade" variant="outlined" />
                    <TextField style={{ width: '100%' }} label="Preço" variant="outlined" />
                    <TextField style={{ width: '100%' }} label="Total" variant="outlined" />
                    <ButtonForm action={submitForm} name="Submit" colour="primary"/>
                </Box>
            </DialogContent>
        </DialogStyled>
    );
}

export default BuyingDialogForm;
