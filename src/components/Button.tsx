import React, { useState } from 'react';
import Button from '@material-ui/core/Button';

interface ButtonFormProps {
    action: () => void;
    name: string;
    colour: any;
};

const ButtonForm: React.FC<ButtonFormProps> = ({ action, name, colour}) => {
    return (
        <Button variant="outlined" color={colour} onClick={action}>
            {name}
        </Button>
    );
};

export default ButtonForm;
