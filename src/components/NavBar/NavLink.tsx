import { Link } from '@material-ui/core';
import React from 'react';

interface NavLink {
    children: React.ReactNode;
    href: string;
}

const NavLink: React.FC<NavLink> = ({ children, href }) => {
    return <Link href={href}>{children}</Link>;
};

export default NavLink;
