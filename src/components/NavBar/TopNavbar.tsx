import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { capitalize } from '../../utils/StringValidator';
import BurgerMenu from './BurgerMenu';

const Nav = styled.nav`
    height: 55px;
    border-bottom: 2px solid #f1f1f1;
    display: flex;
    justify-content: space-between;
`;

const CurrentRouteName = styled.div`
    display: flex;
    align-items: center;
    padding-left: 16px;
    @media (min-width: 768px) {
        width: 15%;
    }
`;

const TopNavbar = () => {
    const [openMenu, setOpenMenu] = useState(false);
    const location = useLocation();
    const currentUrl = String(location.pathname.replace('/', ''));

    const isMobile = window.screen.width < 769;

    const handleMenuBurger = () => {
        setOpenMenu(!openMenu);
    };

    return (
        <Nav style={{ background: 'red' }}>
            <CurrentRouteName>{isMobile ? capitalize(currentUrl) : 'ICON'}</CurrentRouteName>
            <BurgerMenu openMenu={openMenu} onHandleMenuChange={handleMenuBurger} />
        </Nav>
    );
};

export default TopNavbar;
