import React from 'react';
import styled from 'styled-components';
import NavLink from './NavLink';

interface ULProps {
    openMenu: boolean;
}

const Ul = styled.ul<ULProps>`
    list-style: none;
    display: flex;
    flex-flow: row nowrap;
    background-color: yellow;
    margin: 0px;
    width: 100%;
    padding: 0px;
    a {
        align-items: center;
        display: flex;
        justify-content: space-between;
        padding: 0px;
    }
    @media (max-width: 768px) {
        flex-flow: column nowrap;
        background-color: #0d2538;
        position: fixed;
        transform: ${({ openMenu }) => (openMenu ? 'translateX(0)' : 'translateX(100%)')};
        top: 0;
        right: 0;
        height: 100vh;
        width: 300px;
        padding-top: 3.5rem;
        transition: transform 0.3s ease-in-out;
        a {
            color: #fff;
            padding: 18px 10px;
        }
    }
`;

interface RightNavProps {
    openMenu: boolean;
}

const MenuItems: React.FC<RightNavProps> = ({ openMenu }) => {
    const menuOptions = [
        {
            name: 'Home',
            href: '/home'
        },
        {
            name: 'Vendas',
            href: '/vendas'
        },
        {
            name: 'Compras',
            href: '/compras'
        }
    ];

    const listMenuOptions = () =>
        menuOptions.map((link) => (
            <NavLink key={link.href} href={link.href}>
                {link.name}
            </NavLink>
        ));

    return <Ul openMenu={openMenu}>{listMenuOptions()}</Ul>;
};

export default MenuItems;
