import React from 'react';
import styled from 'styled-components';
import MenuItems from './MenuItems';

interface BurgerDivProps {
    openMenu: boolean;
}

const BurgerDiv = styled.div<BurgerDivProps>`
    width: 2rem;
    height: 2rem;
    position: fixed;
    top: 12px;
    right: 20px;
    z-index: 20;
    display: none;
    @media (max-width: 768px) {
        display: flex;
        justify-content: space-around;
        flex-flow: column nowrap;
    }
    div {
        width: 2rem;
        height: 0.25rem;
        background-color: ${({ openMenu }) => (openMenu ? '#ccc' : '#333')};
        border-radius: 10px;
        transform-origin: 1px;
        transition: all 0.3s linear;
        &:nth-child(1) {
            transform: ${({ openMenu }) => (openMenu ? 'rotate(45deg)' : 'rotate(0)')};
        }
        &:nth-child(2) {
            transform: ${({ openMenu }) => (openMenu ? 'translateX(100%)' : 'translateX(0)')};
            opacity: ${({ openMenu }) => (openMenu ? 0 : 1)};
        }
        &:nth-child(3) {
            transform: ${({ openMenu }) => (openMenu ? 'rotate(-45deg)' : 'rotate(0)')};
        }
    }
`;

interface BurgerProps {
    openMenu: boolean;
    onHandleMenuChange: () => void;
}

const BurgerMenu: React.FC<BurgerProps> = ({ openMenu, onHandleMenuChange }) => {
    return (
        <>
            <BurgerDiv openMenu={openMenu} onClick={onHandleMenuChange}>
                <div />
                <div />
                <div />
            </BurgerDiv>
            <MenuItems openMenu={openMenu} />
        </>
    );
};

export default BurgerMenu;
