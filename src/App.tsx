import React from 'react';
import ProjectRoutes from './routes';

export interface IApplicationProps {}

const Application: React.FunctionComponent<IApplicationProps> = (props) => {
    return (
        <div>
            <ProjectRoutes />
        </div>
    );
};

export default Application;
